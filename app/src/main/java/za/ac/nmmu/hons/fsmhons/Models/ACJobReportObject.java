package za.ac.nmmu.hons.fsmhons.Models;

import java.io.Serializable;

/**
 * Created by danie on 8/27/2016.
 */
public class ACJobReportObject implements Serializable{
    public String JobID;
    public String JobDuration;
    public String FaultStatus;
    public String ResponseSummary;
    public String JobRating;
    public String ClientSignature;

    public ACJobReportObject(){

    }

    private ACJobReportObject(String JobID, String JobDuration, String FaultStatus, String ResponseSummary, String JobRating, String ClientSignature)
    {
        this.JobID = JobID;
        this.JobDuration = JobDuration;
        this.FaultStatus = FaultStatus;
        this.ResponseSummary = ResponseSummary;
        this.JobRating = JobRating;
        this.ClientSignature = ClientSignature;
    }

    public String[] toTokened()
    {
        return new String[] { JobID, JobDuration, FaultStatus, ResponseSummary, JobRating, ClientSignature };
    }

    public static ACJobReportObject fromTokened(String[] Tokened)
    {
        return new ACJobReportObject(Tokened[0], Tokened[1], Tokened[2], Tokened[3], Tokened[4], Tokened[5]);
    }
}


