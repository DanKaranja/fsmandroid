package za.ac.nmmu.hons.fsmhons.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.hons.fsmhons.Models.ACJobObject;
import za.ac.nmmu.hons.fsmhons.Models.Communication.TokenedObject;
import za.ac.nmmu.hons.fsmhons.R;
import za.ac.nmmu.hons.fsmhons.Utilities.Persistence;

public class HistoryActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;
    private RecyclerView mRecyclerView;
    private SearchResultsRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(HistoryActivity.this, R.style.AppTheme_Dark_Dialog);
        mRecyclerView = (RecyclerView) findViewById(R.id.History_Recycler);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getBaseContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new SearchResultsRecyclerAdapter();
        mRecyclerView.setAdapter(mAdapter);
        RR_getHistory();
    }
    private void startProcessDialog(String Message) {
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(Message);
        progressDialog.show();
    }
    private void RR_getHistory() {
        startProcessDialog("Fetching...");
        Log.w("HISTORYOUTPUT","Seaching for : "+Persistence.getCurrentJob().ClientDetails[1]);
        Persistence.service.PostQuery(new TokenedObject(Persistence.getToken(),new String[]{"1",Persistence.getCurrentJob().ClientDetails[0],Persistence.getCurrentJob().JobID}))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<String[]>>() {
                    @Override
                    public void onCompleted() {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.w("HISTORYOUTPUT",e.getMessage());
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onNext(ArrayList<String[]> response) {
                        Log.w("HISTORYOUTPUT","Results : "+response.size());
                        if(response != null){ //Valid Token
                            mAdapter.clear();
                            SuperToast.create(getBaseContext(), "# OF RESULTS: "+response.size(), SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                            if (!(response.isEmpty())) {
                                mAdapter.addAll(response);
                            }
                        }
                    }
                });
    }
    private void RR_getHJInstance(String JobID) {
        startProcessDialog("Fetching...");
        Log.w("FETCHOUTPUT","Getting Job : "+JobID);
        Persistence.service.GetHistoryInstance(new TokenedObject(Persistence.getToken(),new String[]{JobID}))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ACJobObject>() {
                    @Override
                    public void onCompleted() {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.w("FETCHOUTPUT",e.getMessage());
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onNext(ACJobObject response) {
                        Log.w("FETCHOUTPUT","Results : Got one!");
                        if(response != null){ //Valid Token
                            Persistence.setTEMPJOBCACHE(response);
                            Intent DetailsIntent = new Intent(getBaseContext(), DetailsActivity.class);
                            startActivity(DetailsIntent);
                        }
                    }
                });
    }

    public class SearchResultsRecyclerAdapter extends RecyclerView.Adapter<SearchResultsRecyclerAdapter.ViewHolder> {
        private ArrayList<String[]> Results;

        public class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.HR_CardView) CardView _HR_CardView;
            @BindView(R.id.HR_Class) TextView _HR_Class;
            @BindView(R.id.HR_Name) TextView _HR_Name;
            @BindView(R.id.HR_Details) TextView _HR_Details;
            @BindView(R.id.HR_IV) ImageView _HR_IV;

            public ViewHolder(View v) {
                super(v);
                ButterKnife.bind(this, v);
            }
        }

        public void addAll(ArrayList<String[]> results) {
            Results.addAll(results);
            notifyDataSetChanged();
        }

        public void clear() {
            Results.clear();
            notifyDataSetChanged();
        }
        public SearchResultsRecyclerAdapter() {
            Results = new ArrayList<>();
        }
        @Override
        public SearchResultsRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_recycler_history, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            final String[] result = Results.get(position);
            holder._HR_IV.setImageResource(R.mipmap.ic_srjob);
            holder._HR_Class.setText(getString(R.string.LBL_SRClass,"Previous Job"));
            holder._HR_Name.setText(getString(R.string.LBL_SRName, result[2]));
            holder._HR_Details.setText(getString(R.string.LBL_SRDetails, result[3]));
            holder._HR_CardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RR_getHJInstance(result[0]);
                }
            });
            holder._HR_CardView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return false;
                }
            });

        }
        @Override
        public int getItemCount() {
            return Results.size();
        }


    }

}
