package za.ac.nmmu.hons.fsmhons.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.hons.fsmhons.Fragments.FragmentListener;
import za.ac.nmmu.hons.fsmhons.Models.ACJobObject;
import za.ac.nmmu.hons.fsmhons.Models.Communication.AuthObject;
import za.ac.nmmu.hons.fsmhons.Models.Communication.HttpResp;
import za.ac.nmmu.hons.fsmhons.Models.Communication.TokenedObject;
import za.ac.nmmu.hons.fsmhons.R;
import za.ac.nmmu.hons.fsmhons.Utilities.JobTimer;
import za.ac.nmmu.hons.fsmhons.Utilities.Persistence;
import za.ac.nmmu.hons.fsmhons.Utilities.Pinger;

public class LandingActivity extends AppCompatActivity implements FragmentListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks,LocationListener {
    private static final int REQUEST_CHECK_SETTINGS = 1;
    private static final int REQUEST_LOCATION_PERMISSION = 1;
    private String REQUESTING_LOCATION_UPDATES_KEY = "LOCATION_UPDATES_KEY";
    private String LOCATION_KEY = "LOCATION_KEY";
    private String LAST_UPDATED_TIME_STRING_KEY = "LAST_UPDATED_TIME_STRING_KEY";
    private String CHECKED_PERMISSION_KEY = "CHECKED_PERMISSION_KEY";
    private RecyclerView mRecyclerView;
    private ScheduleRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private LandingActivity.signInFragment signInFragment;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeContainer;
    private GoogleApiClient mGoogleApiClient;
    private Pinger BackendPinger;
    private Location mCurrentLocation;
    private LocationRequest mLocationRequest;
    private String mLastUpdateTime;
    private boolean mRequestingLocationUpdates = true;
    private boolean mCheckedPermissions = false;
    private MaterialSearchView searchView;
    public static String LatestSearchQuery = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        this.setTitle(getResources().getString(R.string.title_activity_landing));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progressDialog = new ProgressDialog(LandingActivity.this, R.style.AppTheme_Dark_Dialog);
        mRecyclerView = (RecyclerView) findViewById(R.id.schedule_recycler);
        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ScheduleRecyclerAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(this)
                        .color(Color.RED)
                        .sizeResId(R.dimen.divider)
                        .marginResId(R.dimen.margin_left, R.dimen.margin_right)
                        .build());
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.schedules_swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                RR_getSchedule();
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                LatestSearchQuery = query;
                RR_search();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                /*if(newText.length() >= 4) {
                    LatestSearchQuery = newText;
                    RR_search();
                }*/
                return false;
            }
        });
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });
        searchView.setVoiceSearch(true); //or false
        updateValuesFromBundle(savedInstanceState);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    searchView.setQuery(searchWrd, false);
                }
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onResume() {
        super.onResume();
        if (Persistence.Account == "VOID")
        {
            AuthObject account = getPrefAccount();
            if (account != null)
                RR_logIO(account, 1, false);
            else
                launchSignInDialog();
        }
        else
        {
            if(Persistence.isCACHEMPTY())
                RR_getSchedule();
            else
                updateSchedule();
        }
        if(mAdapter != null){
            mAdapter.notifyDataSetChanged();
        }
        if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    private void updateSchedule() {
        mAdapter.clear();
        mAdapter.addAll(Persistence.getCACHE());
    }

    private void startProcessDialog(String Message) {
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(Message);
        progressDialog.show();
    }

    private void removeAccountPreferences() {
        Persistence.Account = "VOID";
        SharedPreferences account = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = account.edit();
        editor.clear();
        editor.commit();
    }

    private void saveToPreferences(AuthObject credentials) {
        SharedPreferences account = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = account.edit();
        editor.putString("AuthID", credentials.AuthID);
        editor.putString("AuthPass", credentials.AuthPass);
        editor.commit();
    }

    private AuthObject getPrefAccount() {
        SharedPreferences account = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String ID = account.getString("AuthID", null);
        String AuthPass = account.getString("AuthPass", null);
        if ((ID == null) || (AuthPass == null))
            return null;
        else
            return (new AuthObject(ID, AuthPass));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_landing, menu);
        MenuItem item = menu.findItem(R.id.action_resources);
        searchView.setMenuItem(item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_account:
                Intent AccountDetailsIntent = new Intent(getBaseContext(), TechnicianAccountActivity.class);
                AccountDetailsIntent.putExtra("TQUERY",new String[]{Persistence.Account,"1",""});// Get details about yourself
                startActivity(AccountDetailsIntent);
                return true;

            case R.id.action_resources:
                Intent DetailsIntent = new Intent(getBaseContext(), SearchActivity.class);
                startActivity(DetailsIntent);
                return true;

            case R.id.action_logout:
                RR_logIO(getPrefAccount(), 0, false);
                RR_getSchedule();
                return true;

            case R.id.action_exit:
                finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onFragmentOptionClick(int option, String FLAG) {
        switch (FLAG) {
            case "SIF": {
                switch (option) {
                    case 0:
                        finish();
                        break;
                    case 1: {
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.response_FailedAuthSignIn), SuperToast.Duration.LONG, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
                        launchSignInDialog();
                    }
                    break;
                    case 2: {
                        launchSignInDialog();
                    }
                    break;
                }
                break;
            }
        }
    }

    @Override
    public void onFragmentObjectClick(int option, Object object, String FLAG) {
        switch (FLAG) {
            case "SIF": {
                switch (option) {
                    case 0: {
                        AuthObject AuthRequest = (AuthObject) object;
                        RR_logIO(AuthRequest, 1, true);
                    }
                    break;
                }
                break;
            }
        }

    }

    public class ScheduleRecyclerAdapter extends RecyclerView.Adapter<ScheduleRecyclerAdapter.ViewHolder> {
        private ArrayList<ACJobObject> Schedule;

        public void addAll(ArrayList<ACJobObject> schedule) {
            Schedule.addAll(schedule);
            notifyDataSetChanged();
        }

        public void clear() {
            Schedule.clear();
            notifyDataSetChanged();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            @BindView(R.id.SCHRYC_CardView) CardView _SCHRYC_CardView;
            @BindView(R.id.SCHRYC_StatusImage) ImageView _SCHRYC_StatusImage;
            @BindView(R.id.SCHRYC_Identifier) TextView _SCHRYC_Identifier;
            @BindView(R.id.SCHRYC_Source) TextView _SCHRYC_Source;
            @BindView(R.id.SCHRYC_Date) TextView _SCHRYC_Date;
            @BindView(R.id.SCHRYC_SupportingText) TextView _SCHRYC_SupportingText;


            public ViewHolder(View v) {
                super(v);
                ButterKnife.bind(this,v);
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public ScheduleRecyclerAdapter() {
            Schedule = new ArrayList<>();
        }

        // Create new views (invoked by the layout manager)
        @Override
        public ScheduleRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_recycler_schedule, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            ACJobObject CurrentJob = (ACJobObject)Schedule.get(position);
            if(CurrentJob.Status.equals(Persistence._SCJ)) {
                if(position == JobTimer.getInstance().getTimedJob()) {
                    holder._SCHRYC_StatusImage.setImageResource(R.drawable.ic_schedule_black_48dp);
                    holder._SCHRYC_CardView.setCardElevation(Float.valueOf("5"));
                }
                else
                    holder._SCHRYC_StatusImage.setImageResource(R.drawable.ic_assignment_black_48dp);
            }
            else
                holder._SCHRYC_StatusImage.setImageResource(R.drawable.ic_assignment_turned_in_black_48dp);
            holder._SCHRYC_Identifier.setText(getString(R.string.LBL_SCHJ_Type, CurrentJob.JobIdentifier));
            holder._SCHRYC_Source.setText(getString(R.string.LBL_SCHJ_Source, CurrentJob.Source));
            holder._SCHRYC_Date.setText(getString(R.string.LBL_SCHJ_Start, CurrentJob.StartDateTime));
            holder._SCHRYC_SupportingText.setText(getString(R.string.LBL_SCHJ_Location, CurrentJob.Location));

            holder._SCHRYC_CardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Persistence.jobIndex = position;
                    Intent DetailsIntent = new Intent(getBaseContext(), DetailsActivity.class);
                    startActivity(DetailsIntent);
                }
            });
        }
        @Override
        public int getItemCount() {
            return Schedule.size();
        }
    }

    //LoginIn Functionality
    private void launchSignInDialog() {
        signInFragment = new signInFragment();
        signInFragment.show(getSupportFragmentManager(), "SIGNIN");
    }

    @SuppressLint("ValidFragment")
    public class signInFragment extends DialogFragment {

        private FragmentListener mListener;
        @BindView(R.id.SIGDIAG_Id) EditText _SIGDIAG_Id;
        @BindView(R.id.SIGDIAG_Pass) EditText _SIGDIAG_Pass;
        public signInFragment() {
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof FragmentListener) {
                mListener = (FragmentListener) context;
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement OnFragmentInteractionListener");
            }

        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            final View DView = inflater.inflate(R.layout.dialog_landing_signin, null);
            ButterKnife.bind(this,DView);
            builder.setView(DView);
            builder.setTitle("Sign in ");
            builder.setPositiveButton(R.string.SignDialog_action_signIn, null);
            builder.setNegativeButton(R.string.SignDialog_action_cancel, null);
            return builder.create();
        }

        @Override
        public void onStart()
        {
            super.onStart();
            final AlertDialog dialog = (AlertDialog)getDialog();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            if(dialog != null)
            {
                Button positiveButton = (Button) dialog.getButton(Dialog.BUTTON_POSITIVE);
                Button negativeButton = (Button) dialog.getButton(Dialog.BUTTON_NEGATIVE);
                positiveButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(Validate()) {
                            dialog.dismiss();
                            mListener.onFragmentObjectClick(0, (new AuthObject(_SIGDIAG_Id.getText().toString(), _SIGDIAG_Pass.getText().toString())), "SIF");
                        }
                    }
                });
                negativeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.onFragmentOptionClick(0, "SIF");
                    }
                });
            }
        }

        public boolean Validate() {
            boolean valid = true;

            String ID = _SIGDIAG_Id.getText().toString();
            String Pass = _SIGDIAG_Pass.getText().toString();

            //TODO: Update the min Length
            if (ID.isEmpty() || ID.length() < 1) {
                _SIGDIAG_Id.setError("Invalid number of characters");
                valid = false;
            } else {
                _SIGDIAG_Id.setError(null);
            }

            if (Pass.isEmpty()) {
                _SIGDIAG_Pass.setError("Password required");
                valid = false;
            } else {
                _SIGDIAG_Pass.setError(null);
            }
            return valid;
        }

    }

    private void RR_logIO(final AuthObject AuthRequest, final int IO, final boolean Fresh){
        startProcessDialog("Processing request...");
        AuthRequest.SignIO = IO;  // 1 = Login or 0 = Logout
        AuthRequest.Header = Persistence.getToken();
        Persistence.service.Authorisation(AuthRequest)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<HttpResp>() {
                    @Override
                    public void onCompleted() {
                        progressDialog.dismiss();
                        if((IO == 1)&& (Persistence.checkToken())) {
                            RR_getSchedule();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        SuperToast.create(getBaseContext(),getResources().getString(R.string.response_FailedNetSignIn), SuperToast.Duration.LONG, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
                        progressDialog.dismiss();
                        onFragmentOptionClick(2, "SIF");
                        e.printStackTrace();
                    }
                    @Override
                    public void onNext(HttpResp response) {
                        if(IO == 1)
                        {
                            if (response.getCheck()) {
                                Persistence.Account = AuthRequest.AuthID;
                                if ((response.protocol.equals(Persistence._IVT))) //invalid token
                                    Persistence.setToken(response.responseString);
                                if (Fresh)
                                    saveToPreferences(AuthRequest);
                                if((response.protocol.equals(Persistence._VT))) {
                                    SuperToast.create(getBaseContext(), "Successfully signed into account: " + AuthRequest.AuthID, SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                                }
                            } else
                                onFragmentOptionClick(1, "SIF");
                        }
                        else
                        {
                            if (response.getCheck()&& (response.protocol.equals(Persistence._VT)))
                            {
                                removeAccountPreferences();
                                launchSignInDialog();
                                SuperToast.create(getBaseContext(), "Successfully signed out from account: " + AuthRequest.AuthID, SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                            } else
                                SuperToast.create(getBaseContext(), "Unable to sign out right now." + AuthRequest.AuthID, SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
                        }
                    }
                });
    }

    private void RR_getSchedule(){
        swipeContainer.setRefreshing(true);
        Persistence.service.GetTechnicianSchedule(new TokenedObject(Persistence.getToken(),new String[]{Persistence.Account}))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<ACJobObject>>() {
                    @Override
                    public void onCompleted() {
                        swipeContainer.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        swipeContainer.setRefreshing(false);
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.response_FailedRequest), SuperToast.Duration.MEDIUM, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();

                    }

                    @Override
                    public void onNext(ArrayList<ACJobObject> response) {
                        if(response != null){ //Valid Token
                            mAdapter.clear();
                            if (!(response.isEmpty())) {
                                Persistence.setCACHE(response);
                                updateSchedule();
                            }
                        }
                        else {
                            SuperToast.create(getBaseContext(), getResources().getString(R.string.response_InvalidToken), SuperToast.Duration.MEDIUM, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
                            launchSignInDialog();
                        }
                    }
                });
    }
    private void RR_search() {
        startProcessDialog("Searching...");
        Log.w("SEARCHOUTPUT","Seaching for : "+LatestSearchQuery);
        Persistence.service.PostQuery(new TokenedObject(Persistence.getToken(),new String[]{"0",LatestSearchQuery}))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<String[]>>() {
                    @Override
                    public void onCompleted() {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.w("SEARCHOUTPUT",e.getMessage());
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onNext(ArrayList<String[]> response) {
                        Log.w("SEARCHOUTPUT","Results : "+response.size());
                        if(response != null){ //Valid Token

                            //updateRefresher(response);
                            Intent resultsIntent = new Intent(getBaseContext(),SearchActivity.class);
                            resultsIntent.putExtra("RESULTS",response);
                            startActivity(resultsIntent);
                        }
                    }
                });
    }
    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    //Location stuff
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY))
            {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(REQUESTING_LOCATION_UPDATES_KEY);
            }
            if (savedInstanceState.keySet().contains(LOCATION_KEY))
            {
                mCurrentLocation = savedInstanceState.getParcelable(LOCATION_KEY);
            }
            if (savedInstanceState.keySet().contains(LAST_UPDATED_TIME_STRING_KEY))
            {
                mLastUpdateTime = savedInstanceState.getString(LAST_UPDATED_TIME_STRING_KEY);
            }
            if (savedInstanceState.keySet().contains(CHECKED_PERMISSION_KEY)) {
                mCheckedPermissions = savedInstanceState.getBoolean(CHECKED_PERMISSION_KEY);
            }
            //BackendPinger.update(geoCodeLocation(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude()),mLastUpdateTime);
        }
    }

    private String geoCodeLocation(double Latitude, double Longitude){
        String geoCodedAddress = "UNKNOWN";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        try {
            List<Address> addresses = geocoder.getFromLocation(Latitude,Longitude, 1);
            if(addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();
                for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(",");
                }
                geoCodedAddress = strReturnedAddress.toString();
            }
            else{
                geoCodedAddress = "NO ADDRESS RETURNED";
            }
        } catch (IOException e) {
            e.printStackTrace();
            geoCodedAddress = "CANNOT DETERMINE ADDRESS";
        }
        return geoCodedAddress;
    }

    protected void createLocationRequest() {
        mLocationRequest  = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if(!mCheckedPermissions)
            SuperToast.create(getBaseContext(), getResources().getString(R.string.response_GooglePlayServicesFailed), SuperToast.Duration.MEDIUM, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
        mCheckedPermissions = true;
        mRequestingLocationUpdates = false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        BackendPinger = Pinger.getInstance();
        createLocationRequest();
        // I HAVE NO IDEA WHAT TF THIS IS FOR, BUT IM JUST GONNA LEAVE IT HERE CAUSE RIGHT NOW, IT WORKS
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        final PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                final LocationSettingsStates states = locationSettingsResult.getLocationSettingsStates();
                switch (status.getStatusCode()){
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(
                                    LandingActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                    }
                }
            });
        // WEIRD UNKNOWN SHIT ENDS HERE
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
        mCheckedPermissions = true;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        //BackendPinger.update(geoCodeLocation(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude()),mLastUpdateTime);
        BackendPinger.update(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude(),mLastUpdateTime);
    }


    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        if(mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onPause() {
        if(mGoogleApiClient.isConnected())
            stopLocationUpdates();
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, mRequestingLocationUpdates);
        savedInstanceState.putBoolean(CHECKED_PERMISSION_KEY, mCheckedPermissions);
        savedInstanceState.putParcelable(LOCATION_KEY, mCurrentLocation);
        savedInstanceState.putString(LAST_UPDATED_TIME_STRING_KEY, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }

    protected void startLocationUpdates() {
        int permissionCheck = ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        else{
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();

                } else {

                    Log.w("MAINOUTPUT", "LOCATION PERMISSION DENIED");
                }
                return;
            }

        }
    }


    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

}
