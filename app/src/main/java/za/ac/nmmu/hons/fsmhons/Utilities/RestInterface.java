package za.ac.nmmu.hons.fsmhons.Utilities;


import java.util.ArrayList;

import retrofit2.http.*;
import rx.Observable;
import za.ac.nmmu.hons.fsmhons.Models.ACJobObject;
import za.ac.nmmu.hons.fsmhons.Models.Communication.AuthObject;
import za.ac.nmmu.hons.fsmhons.Models.Communication.HttpResp;
import za.ac.nmmu.hons.fsmhons.Models.Communication.TokenedObject;

/**
 * Created by danie on 4/2/2016.
 */
public interface RestInterface {
    //Methods related to Authorisation

    @POST("Authorisation")
    Observable<HttpResp> Authorisation(@Body AuthObject param);

    @POST("Schedule")
    Observable<ArrayList<ACJobObject>> GetTechnicianSchedule(@Body TokenedObject param);

    @POST("Ping")
    Observable<HttpResp> PingBackend(@Body TokenedObject param);

    @POST("Inventory")
    Observable<HttpResp> PostInventory(@Body TokenedObject param);

    @POST("Report")
    Observable<HttpResp> PostReport(@Body TokenedObject param);

    @POST("History")
    Observable<ACJobObject> GetHistoryInstance(@Body TokenedObject param);

    @POST("TechnicianAccount")
    Observable<String[]> GetAccountDetails(@Body TokenedObject param);

    @POST("Search")
    Observable<ArrayList<String[]>> PostQuery(@Body TokenedObject param);

}
