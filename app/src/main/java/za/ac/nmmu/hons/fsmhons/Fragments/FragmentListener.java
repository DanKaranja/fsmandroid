package za.ac.nmmu.hons.fsmhons.Fragments;

/**
 * Created by danie on 4/30/2016.
 */
public interface FragmentListener {
    public void onFragmentOptionClick(int option, String FLAG);
    public void onFragmentObjectClick(int option, Object object, String FLAG);
}
