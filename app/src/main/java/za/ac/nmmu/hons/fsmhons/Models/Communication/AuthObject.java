package za.ac.nmmu.hons.fsmhons.Models.Communication;

/**
 * Created by danie on 7/10/2016.
 */
public class AuthObject {
    public int SignIO;
    public String Header;
    public String AuthID;
    public String AuthPass;

    public AuthObject(String AuthID, String authPass) {
        this.AuthID = AuthID;
        this.AuthPass = authPass;
    }
}
