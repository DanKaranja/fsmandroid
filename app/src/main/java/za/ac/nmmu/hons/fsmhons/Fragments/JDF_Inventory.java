package za.ac.nmmu.hons.fsmhons.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScanner;
import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScannerBuilder;
import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.hons.fsmhons.Models.Communication.HttpResp;
import za.ac.nmmu.hons.fsmhons.Models.Communication.TokenedObject;
import za.ac.nmmu.hons.fsmhons.R;
import za.ac.nmmu.hons.fsmhons.Utilities.Persistence;

public class JDF_Inventory extends Fragment{
    private AddInvFragment addInvFragment;
    private RICFragment rICFragment;
    private ProgressDialog progressDialog;
    private RecyclerView mRecyclerView;
    private JobInventoryRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<String[]> Inventory;
    private FragmentListener mListener;
    @BindView(R.id.JDAIF_fab) FloatingActionButton _JDAIF_fab;

    public JDF_Inventory() {
        // Required empty public constructor
    }

    public static JDF_Inventory newInstance() {
        JDF_Inventory fragment = new JDF_Inventory();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_job_inventory, container, false);
        ButterKnife.bind(this, view);
        progressDialog = new ProgressDialog(getActivity().getBaseContext(),R.style.AppTheme_Dark_Dialog);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.JDIF_Recycler);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        Inventory = Persistence.getCurrentJob().InventoryRegistry;
        if(!(Inventory.isEmpty()))
            mAdapter = new JobInventoryRecyclerAdapter(Inventory);
        mRecyclerView.setAdapter(mAdapter);
        if(Persistence.getCurrentJob().Status.equals(Persistence._SCJ)) {
            _JDAIF_fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addInvFragment = new AddInvFragment();
                    addInvFragment.show(getActivity().getSupportFragmentManager(), "ADDINV");
                }
            });
        }else
            _JDAIF_fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_AddressedJob), SuperToast.Duration.MEDIUM, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                }
            });
        return view;
    }
    private void startProcessDialog(String Message) {
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(Message);
        progressDialog.show();
    }

    private void update(String[] Update) {
        //startProcessDialog("Updating");
        Persistence.service.PostInventory(new TokenedObject(Persistence.getToken(),Update))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<HttpResp>() {
                    @Override
                    public void onCompleted() {
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.w("INVENTORY-UPDATE-OUTPUT",e.getMessage());

                    }

                    @Override
                    public void onNext(HttpResp response) {
                        SuperToast.create(getActivity().getBaseContext(), response.responseString, SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                    }
                });
    }

    public class JobInventoryRecyclerAdapter extends RecyclerView.Adapter<JobInventoryRecyclerAdapter.ViewHolder> {
        private ArrayList<String[]> Equipment;

        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            @BindView(R.id.JDI_IRCYV_CardView) CardView _JDI_IRCYV_CardView;
            @BindView(R.id.JDI_IRCYV_Name) TextView _JDI_IRCYV_Name;
            @BindView(R.id.JDI_IRCYV_Comment) TextView _JDI_IRCYV_Comment;
            @BindView(R.id.JDI_IRCYV_ID) TextView _JDI_IRCYV_ID;
            @BindView(R.id.JDI_IRCYV_IV)ImageView _DI_IRCYV_IV;

            public ViewHolder(View v) {
                super(v);
                ButterKnife.bind(this,v);
            }
        }
        public JobInventoryRecyclerAdapter(ArrayList<String[]> Inventory) {

            Equipment = Inventory;
        }
        @Override
        public JobInventoryRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_recycler_inventory, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }
        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            final String[] CurrentEquipment = Equipment.get(position);
            if(CurrentEquipment[3].equals("1"))
                holder._DI_IRCYV_IV.setImageResource(R.drawable.ic_action_action_done_all);
            else
                holder._DI_IRCYV_IV.setImageResource(R.drawable.ic_action_maps_local_shipping);

            holder._JDI_IRCYV_Name.setText(getString(R.string.LBL_InventoryName,CurrentEquipment[1]));
            holder._JDI_IRCYV_Comment.setText(getString(R.string.LBL_InventoryComment,CurrentEquipment[2]));
            holder._JDI_IRCYV_ID.setText(getString(R.string.LBL_InventoryID,CurrentEquipment[0]));
            if(Persistence.getCurrentJob().Status.equals(Persistence._SCJ)) {
                holder._JDI_IRCYV_CardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MaterialBarcodeScanner materialBarcodeScanner = new MaterialBarcodeScannerBuilder()
                                .withActivity(getActivity())
                                .withEnableAutoFocus(true)
                                .withBleepEnabled(true)
                                .withBackfacingCamera()
                                .withText("Scanning equipment barcode")
                                .withResultListener(new MaterialBarcodeScanner.OnResultListener() {
                                    @Override
                                    public void onResult(Barcode barcode) {
                                        Equipment.get(position)[0] = barcode.rawValue;
                                        Equipment.get(position)[3] = "1";
                                        update(new String[]{Persistence.getCurrentJob().JobID,"0",String.valueOf(position),barcode.rawValue});//JobID,update task,Equipment position,barcode
                                    }
                                })
                                .build();
                        materialBarcodeScanner.startScan();
                    }
                });
                holder._JDI_IRCYV_CardView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        rICFragment = new RICFragment(position);
                        rICFragment.show(getActivity().getSupportFragmentManager(), "RICF");
                        return false;
                    }
                });
            }else{
                holder._JDI_IRCYV_CardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_AddressedJob), SuperToast.Duration.MEDIUM, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                    }
                });
            }

        }
        @Override
        public int getItemCount() {
            return Equipment.size();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //Add inventory dialog
    @SuppressLint("ValidFragment")
    public class AddInvFragment extends DialogFragment {

        private FragmentListener mListener;
        @BindView(R.id.AIDIAG_Name) EditText _AIDIAG_Name;
        @BindView(R.id.AIDIAG_Comment) EditText _AIDIAG_Comment;
        public AddInvFragment() {
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof FragmentListener) {
                mListener = (FragmentListener) context;
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement OnFragmentInteractionListener");
            }

        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            final View DView = inflater.inflate(R.layout.dialog_inventoryfragment_addnew, null);
            ButterKnife.bind(this,DView);
            builder.setView(DView);
            builder.setTitle("Register Equipment ");
            builder.setPositiveButton(R.string.AddInventoryDialog_action_Scan, null);
            builder.setNegativeButton(R.string.action_CancelTask, null);
            return builder.create();
        }

        @Override
        public void onStart()
        {
            super.onStart();
            final AlertDialog dialog = (AlertDialog)getDialog();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            if(dialog != null)
            {
                Button positiveButton = (Button) dialog.getButton(Dialog.BUTTON_POSITIVE);
                Button negativeButton = (Button) dialog.getButton(Dialog.BUTTON_NEGATIVE);
                positiveButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(Validate()) {
                            final MaterialBarcodeScanner materialBarcodeScanner = new MaterialBarcodeScannerBuilder()
                                    .withActivity(getActivity())
                                    .withEnableAutoFocus(true)
                                    .withBleepEnabled(true)
                                    .withBackfacingCamera()
                                    .withText("Scanning equipment barcode")
                                    .withResultListener(new MaterialBarcodeScanner.OnResultListener() {
                                        @Override
                                        public void onResult(Barcode barcode) {
                                            Persistence.getCurrentJob().InventoryRegistry.add(new String[]{barcode.rawValue,_AIDIAG_Name.getText().toString(),_AIDIAG_Comment.getText().toString(),"1"});
                                            update(new String[]{Persistence.getCurrentJob().JobID,"1",barcode.rawValue,_AIDIAG_Name.getText().toString(),_AIDIAG_Comment.getText().toString()});//JobID,update task,Equipment position,barcode
                                        }
                                    })
                                    .build();
                            materialBarcodeScanner.startScan();
                            dialog.dismiss();
                        }
                    }
                });
                negativeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        }

        public boolean Validate() {
            boolean valid = true;

            String EName = _AIDIAG_Name.getText().toString();
            String EComment = _AIDIAG_Comment.getText().toString();

            if (EName.length() < 1) {
                _AIDIAG_Name.setError("Equipment name cannot be empty");
                valid = false;
            } else {
                _AIDIAG_Name.setError(null);
            }

            if (EComment.length() < 1) {
                _AIDIAG_Comment.setError("Please add a comment");
                valid = false;
            } else {
                _AIDIAG_Comment.setError(null);
            }

            return valid;
        }

    }


    //Remove inventory check dialog
    @SuppressLint("ValidFragment")
    public class RICFragment extends DialogFragment {

        private FragmentListener mListener;
        private int position;
        public RICFragment(int position) {
            this.position = position;
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof FragmentListener) {
                mListener = (FragmentListener) context;
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement OnFragmentInteractionListener");
            }

        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            String[] curEquipment = Persistence.getCurrentJob().InventoryRegistry.get(position);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Are you sure you would want to deregister "+curEquipment[2]+"("+curEquipment[1]+") from the current job's inventory?")
                    .setPositiveButton(R.string.RICDialog_action_Remove, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Persistence.getCurrentJob().InventoryRegistry.get(position)[0] = "-";
                            Persistence.getCurrentJob().InventoryRegistry.get(position)[3] = "0";
                            update(new String[]{Persistence.getCurrentJob().JobID,"-1",String.valueOf(position)});
                            SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.action_Done), SuperToast.Duration.SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                        }
                    })
                    .setNegativeButton(R.string.action_No, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            return builder.create();
        }



    }

}
