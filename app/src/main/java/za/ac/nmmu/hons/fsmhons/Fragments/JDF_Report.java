package za.ac.nmmu.hons.fsmhons.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.hons.fsmhons.Models.ACJobReportObject;
import za.ac.nmmu.hons.fsmhons.Models.Communication.AuthObject;
import za.ac.nmmu.hons.fsmhons.Models.Communication.HttpResp;
import za.ac.nmmu.hons.fsmhons.Models.Communication.TokenedObject;
import za.ac.nmmu.hons.fsmhons.R;
import za.ac.nmmu.hons.fsmhons.Utilities.JobTimer;
import za.ac.nmmu.hons.fsmhons.Utilities.Persistence;

public class JDF_Report extends Fragment {
    @BindView(R.id.JDRF_Spinner) Spinner _JDRF_Spinner;
    @BindView(R.id.TXT_JDRF_BriefSum) EditText _TXT_JDRF_BriefSum;
    @BindView(R.id.LBL_JDRF_Duration) TextView _LBL_JDRF_Duration;
    @BindView(R.id.BTN_FeedSubmit) Button _BTN_FeedSubmit;
    private ProgressDialog progressDialog;
    private Thread uiUpdaterThread;

    public JDF_Report() {
        // Required empty public constructor
    }
    public static JDF_Report newInstance() {
        JDF_Report fragment = new JDF_Report();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_job_report, container, false);
        ButterKnife.bind(this, view);
        progressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_Dark_Dialog);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity().getBaseContext(), R.array.fault_status, R.layout.custom_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        _JDRF_Spinner.setAdapter(adapter);

        if(Persistence.getCurrentJob().Status.equals(Persistence._SCJ)) //Check if its an unaddressed job
        {
            _LBL_JDRF_Duration.setText(getString(R.string.LBL_JDRF_JobDuration, JobTimer.getInstance().getTime()));
            startTimerUpdater();//Update the UI as the timer counts
            _BTN_FeedSubmit.setText("SUBMIT REPORT");
            _BTN_FeedSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Validate()) {
                        ACJobReportObject nReport = new ACJobReportObject();
                        nReport.JobID = Persistence.getCurrentJob().JobID;
                        nReport.JobDuration = JobTimer.getInstance().getTime();
                        nReport.ResponseSummary = _TXT_JDRF_BriefSum.getText().toString();
                        switch (_JDRF_Spinner.getSelectedItemPosition()) {
                            case 0:
                                nReport.FaultStatus = Persistence._AJ;//Job Addressed
                                break;
                            case 1:
                                nReport.FaultStatus = Persistence._JRF;//Job Requires followup
                                break;
                        }
                        SubmitReportFragment SMRF= new SubmitReportFragment();
                        Bundle args = new Bundle();
                        args.putSerializable("REPORT",nReport);
                        SMRF.setArguments(args);
                        SMRF.show(getActivity().getSupportFragmentManager(), "SUBMIT-REPORT");
                    }

                }
            });
        }
        else{
            String[] Rep = Persistence.getCurrentJob().JobReport;
            final ACJobReportObject Report = ACJobReportObject.fromTokened(Rep);
            _LBL_JDRF_Duration.setText(getString(R.string.LBL_JDRF_JobDuration, Report.JobDuration));
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getActivity().getBaseContext(), R.layout.custom_spinner_item, new String[]{Report.FaultStatus});
            _JDRF_Spinner.setAdapter(spinnerAdapter);
            _TXT_JDRF_BriefSum.setText(Report.ResponseSummary);
            _TXT_JDRF_BriefSum.setEnabled(false);
            _BTN_FeedSubmit.setText("VIEW FEEDBACK");
            _BTN_FeedSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SubmitReportFragment SMRF= new SubmitReportFragment();
                    Bundle args = new Bundle();
                    args.putSerializable("REPORT",Report);
                    SMRF.setArguments(args);
                    SMRF.show(getActivity().getSupportFragmentManager(), "SUBMIT-REPORT");
                }
            });
        }
        return view;
    }

    public void startTimerUpdater(){
        uiUpdaterThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(JobTimer.TimerStep);
                        if(getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    _LBL_JDRF_Duration.setText(getString(R.string.LBL_JDRF_JobDuration, JobTimer.getInstance().getTime()));
                                }
                            });
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        uiUpdaterThread.start();

    }

    @Override
    public void onDestroyView() {
        if(uiUpdaterThread != null)
            uiUpdaterThread.interrupt();
        super.onDestroyView();
    }

    public boolean Validate() {
        boolean valid = true;
        String summaryResponse = _TXT_JDRF_BriefSum.getText().toString();
        if(JobTimer.getInstance().getTime() != null){
            if (summaryResponse.isEmpty()) {
                _TXT_JDRF_BriefSum.setError("A response summary is required to submit a report");
                valid = false;
            } else {
                _TXT_JDRF_BriefSum.setError(null);
            }
        }
        else {
            valid = false;
            SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_validateTimed), SuperToast.Duration.SHORT, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
        }


        return valid;
    }

    private void startProcessDialog(String Message) {
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(Message);
        progressDialog.show();
    }

    @SuppressLint("ValidFragment")
    public class SubmitReportFragment extends DialogFragment {

        private FragmentListener mListener;
        private boolean mCheckSigned = false;
        @BindView(R.id.JDRF_CF_Ratings) RatingBar _JDRF_CF_Ratings;
        private SignaturePad mSignaturePad;
        private ACJobReportObject Report;
        public SubmitReportFragment() {
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof FragmentListener) {
                mListener = (FragmentListener) context;
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement OnFragmentInteractionListener");
            }

        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            final View DView = inflater.inflate(R.layout.dialog_report_feedback, null);
            ButterKnife.bind(this,DView);
            builder.setView(DView);
            builder.setPositiveButton(R.string.BTN_JDRF_Submit, null);
            builder.setNegativeButton(R.string.action_CancelTask, null);
            mSignaturePad = (SignaturePad) DView.findViewById(R.id.signature_pad);
            Report = (ACJobReportObject) getArguments().getSerializable("REPORT");
            if(Report == null)
                dismiss();
            if(Persistence.getCurrentJob().Status.equals(Persistence._SCJ)) //Check if its an unaddressed job
            {
                mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                    @Override
                    public void onStartSigning() {
                    }

                    @Override
                    public void onSigned() {
                        //SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_reportSigning), SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.POPUP)).show();
                        mCheckSigned = true;
                    }

                    @Override
                    public void onClear() {
                        //Event triggered when the pad is cleared

                    }
                });
                _JDRF_CF_Ratings.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                        mSignaturePad.clear();
                        //SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_reportCleared), SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.POPUP)).show();
                    }
                });
            }
            else
            {
                _JDRF_CF_Ratings.setRating(Float.valueOf(Report.JobRating));
                _JDRF_CF_Ratings.setEnabled(false);
                mSignaturePad.setSignatureBitmap(Persistence.decodeImgBase64(Report.ClientSignature));
                mSignaturePad.setEnabled(false);
            }
            return builder.create();
        }

        @Override
        public void onStart()
        {
            super.onStart();
            final AlertDialog dialog = (AlertDialog)getDialog();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(true);
            if(dialog != null) {
                Button positiveButton = (Button) dialog.getButton(Dialog.BUTTON_POSITIVE);
                Button negativeButton = (Button) dialog.getButton(Dialog.BUTTON_NEGATIVE);
                if (Persistence.getCurrentJob().Status.equals(Persistence._SCJ)) //Check if its an unaddressed job
                {
                    positiveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Persistence.getCurrentJob().Status.equals(Persistence._SCJ)) //Check if its an unaddressed job
                            {
                                Report.JobRating = String.valueOf(_JDRF_CF_Ratings.getRating());
                                if (mCheckSigned) {
                                    Report.ClientSignature = Persistence.encodeImgToBase64(mSignaturePad.getSignatureBitmap(), Bitmap.CompressFormat.JPEG, 100);
                                    RR_submitReport(Report.toTokened());
                                } else {
                                    SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_validateSignature), SuperToast.Duration.SHORT, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
                                }
                            } else {
                                SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_AddressedJob), SuperToast.Duration.MEDIUM, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                            }
                        }
                    });
                    negativeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dismiss();
                        }
                    });

                }
            }
        }


        private void RR_submitReport(final String[] preTokenedReport){
            startProcessDialog("Submitting report...");
            Persistence.service.PostReport(new TokenedObject(Persistence.getToken(),preTokenedReport))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<HttpResp>() {
                        @Override
                        public void onCompleted() {
                            progressDialog.dismiss();
                            Persistence.getCurrentJob().Status = preTokenedReport[2]; //Update local status of the job
                            Persistence.getCurrentJob().JobReport = preTokenedReport;
                            JobTimer.getInstance().stopTimer();
                            SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_AddressedJob), SuperToast.Duration.MEDIUM, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                            getActivity().finish();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_FailedRequest), SuperToast.Duration.MEDIUM, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();

                        }

                        @Override
                        public void onNext(HttpResp response) {
                            if(response.getCheck()){ //Valid Token
                                SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_reportLodged), SuperToast.Duration.MEDIUM, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                            }
                            else
                                SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_FailedRequest), SuperToast.Duration.MEDIUM, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();

                        }
                    });
        }

    }



}
