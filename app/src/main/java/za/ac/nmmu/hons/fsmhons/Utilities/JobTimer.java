package za.ac.nmmu.hons.fsmhons.Utilities;

/**
 * Created by s212307584 on 2016/08/24.
 */
import android.content.Intent;
import android.support.v4.util.Pair;
import android.text.format.Time;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class JobTimer {
    private static JobTimer Instance;
    public static final Integer TimerStep = 1000;
    private Integer TimerStatus = -1;
    private Integer TrackingJob = -1;
    private DateFormat dateFormat = new java.text.SimpleDateFormat("hh:mm:ss");
    private ArrayList<Long> Laps = new ArrayList<>();
    private Date onStart;

    private JobTimer(){

    }

    public static JobTimer getInstance(){
        if(Instance == null) // No Timer is running
            Instance = new JobTimer();
        return Instance;
    }

    private boolean isFresh(){
        return (TrackingJob == -1);
    }

    public boolean isTimed(){
        return (Persistence.jobIndex == TrackingJob);
    }

    public Integer getTimedJob(){
        return TrackingJob;
    }

    public Integer getTimerStatus(){
        return TimerStatus;
    }


    private void toggleTimerStatus(String Status){
        switch (Status) {
            case "#NEW":
                TrackingJob = Persistence.jobIndex;
                break;
            case "#STOP":
                TrackingJob = -1;
                TimerStatus = -1;
                break;
            case "#START":
                TimerStatus = 1;
                break;
            case "#PAUSE":
                TimerStatus = 0;
                break;
        }
    }
    public boolean startTimer() {
        boolean isFresh = isFresh();
        if(isFresh)
        {
            toggleTimerStatus("#NEW");
            onStart = getCurrentTime();
            toggleTimerStatus("#START");
            Log.w("TIMEROUTPUT", "#STARTED: " + TrackingJob);
        }
        return isFresh;
    }

    public void pauseTimer(){
        if(isTimed()) {
            Date onPause = getCurrentTime();
            Laps.add(onPause.getTime() - onStart.getTime());
            toggleTimerStatus("#PAUSE");
            Log.w("TIMEROUTPUT", "#PAUSED: "+TrackingJob);
        }
    }

    public void resumeTimer(){
        if(isTimed()){
            onStart = getCurrentTime();
            toggleTimerStatus("#START");
            Log.w("TIMEROUTPUT", "#RESUMED: "+TrackingJob);
        }

    }

    public void stopTimer(){
        if(isTimed()) {
            Laps.add(onStart.getTime() - getCurrentTime().getTime());
            toggleTimerStatus("#STOP");
            Log.w("TIMEROUTPUT", "#STOPPED: "+TrackingJob);
        }
    }
    private Date getCurrentTime(){
        Date output = null;
        Calendar calendar = Calendar.getInstance();
        int HH = calendar.get(Calendar.HOUR);
        int MM = calendar.get(Calendar.MINUTE);
        int SS = calendar.get(Calendar.SECOND);
        try {
            output =  dateFormat.parse(HH+":"+MM+":"+SS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output;
    }
    private Long getTotalTime(){
        Long total = Long.valueOf(0);
        for(Long ms : Laps){
            total += ms;
        }
        return total;
    }

    private String formatMS(Long Miliseconds){
        Integer hours = (Miliseconds.intValue()/1000) / 3600;
        Integer minutes = ((Miliseconds.intValue()/1000) % 3600) / 60;
        Integer seconds = (Miliseconds.intValue()/1000) % 60;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public String getTime(){
        if(isTimed())
        {
            switch (TimerStatus){
                default:
                    return "-";
                case 0:
                    return formatMS(getTotalTime());
                case 1:{
                    return formatMS(getTotalTime() + (getCurrentTime().getTime() - onStart.getTime()));
                }
            }
        }
        else
            return null;
    }


}


