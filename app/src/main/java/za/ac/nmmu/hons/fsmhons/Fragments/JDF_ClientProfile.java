package za.ac.nmmu.hons.fsmhons.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;

import butterknife.BindView;
import butterknife.ButterKnife;
import za.ac.nmmu.hons.fsmhons.R;
import za.ac.nmmu.hons.fsmhons.Utilities.Persistence;

public class JDF_ClientProfile extends Fragment implements View.OnClickListener {
    private FragmentListener mListener;
    private ProgressDialog progressDialog;
    @BindView(R.id.LBL_TXTJDCPF_Name) TextView _LBL_TXTJDCPF_Name;
    @BindView(R.id.LBL_TXTJDCPF_Company) TextView _LBL_TXTJDCPF_Company;
    @BindView(R.id.LBL_TXTJDCPF_eAddress) TextView _LBL_TXTJDCPF_eAddress;
    @BindView(R.id.LBL_TXTJDCPF_Contact) TextView _LBL_TXTJDCPF_Contact;
    @BindView(R.id.LBL_TXTJDCPF_pAddress) TextView _LBL_TXTJDCPF_pAddress;
    @BindView(R.id.IVS_JDCPF_eAddress) ImageView _IVS_JDCPF_eAddress;
    @BindView(R.id.IVS_JDCPF_Contact) ImageView _IVS_JDCPF_Contact;
    @BindView(R.id.IVS_JDCPF_pAddress) ImageView _IVS_JDCPF_pAddress;

    public JDF_ClientProfile() {
        // Required empty public constructor
    }

    public static JDF_ClientProfile newInstance() {
        JDF_ClientProfile fragment = new JDF_ClientProfile();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_job_clientprofile, container, false);
        ButterKnife.bind(this, view);
        progressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_Dark_Dialog);
        UpdateDetails();
        _IVS_JDCPF_eAddress.setOnClickListener(this);
        _IVS_JDCPF_Contact.setOnClickListener(this);
        _IVS_JDCPF_pAddress.setOnClickListener(this);
        return view;
    }

    private void UpdateDetails()
    {
        _LBL_TXTJDCPF_Name.setText(Persistence.getCurrentJob().ClientDetails[1]);
        _LBL_TXTJDCPF_Company.setText(Persistence.getCurrentJob().ClientDetails[2]);
        _LBL_TXTJDCPF_eAddress.setText(Persistence.getCurrentJob().ClientDetails[3]);
        _LBL_TXTJDCPF_Contact.setText(Persistence.getCurrentJob().ClientDetails[4]);
        _LBL_TXTJDCPF_pAddress.setText(Persistence.getCurrentJob().ClientDetails[5]);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.IVS_JDCPF_eAddress:
            {
                String eAddress = _LBL_TXTJDCPF_eAddress.getText().toString();
                if(!(eAddress.equals("-"))) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("*/*");
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{eAddress});
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }
            }
            break;
            case R.id.IVS_JDCPF_Contact:
            {
                String Contact = _LBL_TXTJDCPF_Contact.getText().toString();
                if(!(Contact.equals("-"))) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + Contact));
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(intent);
                    }

                }
            }
            break;
            case R.id.IVS_JDCPF_pAddress:
            {
                String pAddress = _LBL_TXTJDCPF_pAddress.getText().toString();
                if(!(pAddress.equals("-"))) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("google.navigation:q="+pAddress+"=d"));
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(intent);
                    }else
                        SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_FailedMapResolve), SuperToast.Duration.MEDIUM, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();

                }
            }
            break;

        }
    }
}
