package za.ac.nmmu.hons.fsmhons.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.hons.fsmhons.Fragments.FragmentListener;
import za.ac.nmmu.hons.fsmhons.Fragments.JDF_ClientProfile;
import za.ac.nmmu.hons.fsmhons.Fragments.JDF_Documents;
import za.ac.nmmu.hons.fsmhons.Fragments.JDF_Information;
import za.ac.nmmu.hons.fsmhons.Fragments.JDF_Inventory;
import za.ac.nmmu.hons.fsmhons.Fragments.JDF_Report;
import za.ac.nmmu.hons.fsmhons.Models.Communication.TokenedObject;
import za.ac.nmmu.hons.fsmhons.R;
import za.ac.nmmu.hons.fsmhons.Utilities.JobTimer;
import za.ac.nmmu.hons.fsmhons.Utilities.Persistence;

public class DetailsActivity extends AppCompatActivity implements FragmentListener{

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private MaterialSearchView searchView;
    public static String LatestSearchQuery = "";
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        progressDialog = new ProgressDialog(DetailsActivity.this, R.style.AppTheme_Dark_Dialog);
        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                LatestSearchQuery = query;
                RR_search();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                /*if(newText.length() >= 4) {
                    LatestSearchQuery = newText;
                    RR_search();
                }*/
                return false;
            }
        });
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });
        searchView.setVoiceSearch(true); //or false

        setupTabIcons();
        if(Persistence.jobIndex == -1){
            SuperToast.create(getBaseContext(), getResources().getString(R.string.response_JobLoadFailed), SuperToast.Duration.MEDIUM, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
            finish();
        }

    }
    private void startProcessDialog(String Message) {
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(Message);
        progressDialog.show();
    }
    private void RR_search() {
        startProcessDialog("Searching...");
        Log.w("SEARCHOUTPUT","Seaching for : "+LatestSearchQuery);
        Persistence.service.PostQuery(new TokenedObject(Persistence.getToken(),new String[]{"0",LatestSearchQuery}))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<String[]>>() {
                    @Override
                    public void onCompleted() {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.w("SEARCHOUTPUT",e.getMessage());
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onNext(ArrayList<String[]> response) {
                        Log.w("SEARCHOUTPUT","Results : "+response.size());
                        if(response != null){ //Valid Token

                            //updateRefresher(response);
                            Intent resultsIntent = new Intent(getBaseContext(),SearchActivity.class);
                            resultsIntent.putExtra("RESULTS",response);
                            startActivity(resultsIntent);
                        }
                    }
                });
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_info_white_48dp); // Job info
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_account_box_white_48dp); //Client profile
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_attachment_white_48dp); //Attachments
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_build_white_48dp); //InventoryRegistry
        tabLayout.getTabAt(4).setIcon(R.drawable.ic_book_white_48dp); //End report
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        MenuItem item = menu.findItem(R.id.action_resources);
        searchView.setMenuItem(item);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_resources:
                Intent SearchIntent = new Intent(getBaseContext(), SearchActivity.class);
                startActivity(SearchIntent);
                return true;

            case R.id.action_history:
                Intent HistoryIntent = new Intent(getBaseContext(),HistoryActivity.class);
                startActivity(HistoryIntent);
                return true;

            case android.R.id.home:
                Intent intent = NavUtils.getParentActivityIntent(this);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                NavUtils.navigateUpTo(this, intent);
                return true;
        }
        // If we got here, the user's action was not recognized.
        // Invoke the superclass to handle it.
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentOptionClick(int option, String FLAG) {

    }

    @Override
    public void onFragmentObjectClick(int option, Object object, String FLAG) {

    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position)
            {
                case 1:
                    return JDF_ClientProfile.newInstance();
                case 2:
                    return JDF_Documents.newInstance();
                case 3:
                    return JDF_Inventory.newInstance();
                case 4:
                    return JDF_Report.newInstance();
            }
            return JDF_Information.newInstance();
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "DETAILS";
                case 1:
                    return "CLIENT";
                case 2:
                    return "RESOURCES";
                case 3:
                    return "INVENTORY";
                case 4:
                    return "REPORT";
            }
            return null;
        }
    }



}
