package za.ac.nmmu.hons.fsmhons.Models.Communication;

/**
 * Created by danie on 4/24/2016.
 */
public class HttpResp {
    public int check;
    public String protocol;
    public String responseString;

    public HttpResp(int check, String protocol) {
        this.check = check;
        this.protocol = protocol;
    }
    public HttpResp(int check, String protocol,String responseString)
    {
        this.check = check;
        this.protocol = protocol;
        this.responseString = responseString;
    }

    public boolean getCheck() {
        return (check == 1);
    }


}
