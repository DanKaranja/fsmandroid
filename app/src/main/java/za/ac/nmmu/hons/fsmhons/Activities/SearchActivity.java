package za.ac.nmmu.hons.fsmhons.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.UiThread;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.hons.fsmhons.Fragments.FragmentListener;
import za.ac.nmmu.hons.fsmhons.Models.ACJobObject;
import za.ac.nmmu.hons.fsmhons.Models.Communication.TokenedObject;
import za.ac.nmmu.hons.fsmhons.R;
import za.ac.nmmu.hons.fsmhons.Utilities.Persistence;

public class SearchActivity extends AppCompatActivity {
    public static HashMap<String,ArrayList<String[]>> SearchResults = new HashMap<>();
    public static String LatestSearchQuery = "";
    private ResultsUpdater mListener;
    private ProgressDialog progressDialog;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    @BindView(R.id.container) ViewPager mViewPager;
    @BindView(R.id.tabs) TabLayout tabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        progressDialog = new ProgressDialog(SearchActivity.this, R.style.AppTheme_Dark_Dialog);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            SuperToast.create(getBaseContext(), "Unable to retrieve results", SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
            finish();
        }
        ArrayList<String[]> results = (ArrayList<String[]>) extras.getSerializable("RESULTS");
        if (results != null) {
            SuperToast.create(getBaseContext(), "# OF RESULTS: "+results.size(), SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
            updateRefresher(results);
        }
       // setupTabIcons();
    }
    /* private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.mipmap.ic_srjob); // Previous Job
        tabLayout.getTabAt(1).setIcon(R.mipmap.ic_srtech);// Technician
        tabLayout.getTabAt(2).setIcon(R.mipmap.ic_srinv); // Inventory
    }*/

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int SR_TYPE) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            switch (SR_TYPE)
            {
                default: {
                    Fragment instance = PlaceholderFragment.newInstance(Persistence._SR_JOB);
                    mListener = (ResultsUpdater) instance;
                    return instance;
                }
                case 1: {
                    Fragment instance =  PlaceholderFragment.newInstance(Persistence._SR_TECH);
                    mListener = (ResultsUpdater) instance;
                    return instance;
                }
                case 2: {
                    Fragment instance =   PlaceholderFragment.newInstance(Persistence._SR_INV);
                    mListener = (ResultsUpdater) instance;
                    return instance;
                }

            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "PREVIOUS JOBS";
                case 1:
                    return "TECHNICIANS";
                case 2:
                    return "INVENTORY";
            }


            return null;
        }


    }
    public static class PlaceholderFragment extends Fragment implements ResultsUpdater{
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private RecyclerView mRecyclerView;
        private RecyclerView.LayoutManager mLayoutManager;
        private SearchResultsRecyclerAdapter mAdapter;
        private ProgressDialog progressDialog;
        private static final String ARG_RESULT_TYPE = "results_type";
        private String SRF_TYPE;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(String SR_TYPE) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString(ARG_RESULT_TYPE, SR_TYPE);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_search, container, false);
            SRF_TYPE = getArguments().getString(ARG_RESULT_TYPE);
            progressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_Dark_Dialog);
            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.Search_Recycler);
            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            if(SearchResults.containsKey(SRF_TYPE)){
                mAdapter = new SearchResultsRecyclerAdapter(SearchResults.get(SRF_TYPE));
                mRecyclerView.setAdapter(mAdapter);
            }
            return rootView;
        }

        private void startProcessDialog(String Message) {
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(Message);
            progressDialog.show();
        }

        @Override
        public void Updated() {
            SuperToast.create(getActivity().getBaseContext(), "Update sent!", SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
            if(SearchResults.containsKey(SRF_TYPE)){
                mAdapter = new SearchResultsRecyclerAdapter(SearchResults.get(SRF_TYPE));
                mRecyclerView.setAdapter(mAdapter);
            }
        }

        public class SearchResultsRecyclerAdapter extends RecyclerView.Adapter<SearchResultsRecyclerAdapter.ViewHolder> {
            private ArrayList<String[]> Results;

            public class ViewHolder extends RecyclerView.ViewHolder {
                @BindView(R.id.SR_CardView) CardView _SR_CardView;
                @BindView(R.id.SR_Class) TextView _SR_Class;
                @BindView(R.id.SR_Name) TextView _SR_Name;
                @BindView(R.id.SR_Details) TextView _SR_Details;
                @BindView(R.id.SR_IV) ImageView _SR_IV;

                public ViewHolder(View v) {
                    super(v);
                    ButterKnife.bind(this, v);
                }
            }
            public SearchResultsRecyclerAdapter(ArrayList<String[]> Update) {
                this.Results = Update;
            }
            @Override
            public SearchResultsRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_recycler_search_results, parent, false);
                ViewHolder vh = new ViewHolder(v);
                return vh;
            }
            @Override
            public void onBindViewHolder(final ViewHolder holder, final int position) {
                final String[] result = Results.get(position);
                if(result[1].equals(Persistence._SR_INV))
                {
                    holder._SR_IV.setImageResource(R.mipmap.ic_srinv);
                    holder._SR_Class.setText(getString(R.string.LBL_SRClass,"Inventory"));
                    holder._SR_Name.setText(getString(R.string.LBL_SRName, result[2]));
                    holder._SR_Details.setText(getString(R.string.LBL_SRDetails, result[3]));
                    holder._SR_CardView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent AccountDetailsIntent = new Intent(getActivity().getBaseContext(), TechnicianAccountActivity.class);
                            AccountDetailsIntent.putExtra("TQUERY",new String[]{result[0],"1",LatestSearchQuery});
                            startActivity(AccountDetailsIntent);
                        }
                    });
                }
                if(result[1].equals(Persistence._SR_JOB))
                {
                    holder._SR_IV.setImageResource(R.mipmap.ic_srjob);
                    holder._SR_Class.setText(getString(R.string.LBL_SRClass,"Previous Job"));
                    holder._SR_Name.setText(getString(R.string.LBL_SRName, result[2]));
                    holder._SR_Details.setText(getString(R.string.LBL_SRDetails, result[3]));
                    holder._SR_CardView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            RR_getHJInstance(result[0]);
                        }
                    });
                }
                if(result[1].equals(Persistence._SR_TECH))
                {
                    holder._SR_IV.setImageResource(R.mipmap.ic_srtech);
                    holder._SR_Class.setText(getString(R.string.LBL_SRClass,"Technician"));
                    holder._SR_Name.setText(getString(R.string.LBL_SRName, result[2]));
                    holder._SR_Details.setText(getString(R.string.LBL_SRDetails, result[3]));
                    holder._SR_CardView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent AccountDetailsIntent = new Intent(getActivity().getBaseContext(), TechnicianAccountActivity.class);
                            AccountDetailsIntent.putExtra("TQUERY",new String[]{result[0],"1",LatestSearchQuery}); // Get details about other technician
                            startActivity(AccountDetailsIntent);
                        }
                    });
                }
            }
            @Override
            public int getItemCount() {
                return Results.size();
            }


        }

        private void RR_getHJInstance(String JobID) {
            startProcessDialog("Fetching...");
            Log.w("FETCHOUTPUT","Getting Job : "+JobID);
            Persistence.service.GetHistoryInstance(new TokenedObject(Persistence.getToken(),new String[]{JobID}))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<ACJobObject>() {
                        @Override
                        public void onCompleted() {
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.w("FETCHOUTPUT",e.getMessage());
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onNext(ACJobObject response) {
                            Log.w("FETCHOUTPUT","Results : Got one!");
                            if(response != null){ //Valid Token
                                Persistence.setTEMPJOBCACHE(response);
                                Intent DetailsIntent = new Intent(getActivity().getBaseContext(), DetailsActivity.class);
                                startActivity(DetailsIntent);
                            }
                        }
                    });
        }
    }

    private void updateRefresher(ArrayList<String[]> response){
        SearchResults.clear();
        if (!(response.isEmpty())) {
            for(String[] SR : response){
                String SRF_TYPE = SR[1];
                if(SearchResults.containsKey(SRF_TYPE))
                    SearchResults.get(SRF_TYPE).add(SR);
                else {
                    ArrayList<String[]> temp = new ArrayList<>();
                    temp.add(SR);
                    SearchResults.put(SRF_TYPE,temp);
                }
            }
            //mListener.Updated();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = NavUtils.getParentActivityIntent(this);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                NavUtils.navigateUpTo(this, intent);
                return true;
        }
        // If we got here, the user's action was not recognized.
        // Invoke the superclass to handle it.
        return super.onOptionsItemSelected(item);
    }



    public interface ResultsUpdater{
        void Updated();
    }


}
