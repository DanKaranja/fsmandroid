package za.ac.nmmu.hons.fsmhons.Utilities;


import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import za.ac.nmmu.hons.fsmhons.Models.ACJobObject;

/**
 * Created by danie on 4/24/2016.
 */
public class Persistence {

    //Settings and preferences
    private static String NMMUBASEURL = "http://fsmhons.csdev.nmmu.ac.za";
    private static String NMMUTESTURL = "http://192.168.56.1";
    private static String TESTBASEURL = "http://10.0.0.3:58949";
    private static Crypto Cryptor = new Crypto();
    private static String Token = "INVALID";
    private static String CRYPTOKEY = "72836RH47629RU463H7287E73464";
    //Protocol strings
    public static String _PRESP = "PING-RESPONSE"; //PROTOCOL - PING RESPONSE
    public static String _HTTPRESP = "HTTPREQUEST-RESPONSE"; //PROTOCOL - httpResp RESPONSE
    public static String _NR = "AS-IVT"; //PROTOCOL - NULL REQUEST
    public static String _VT = "AS-VT"; //PROTOCOL - VALID TOKEN
    public static String _IVT = "AS-IVT"; //PROTOCOL - INVALID TOKEN
    public static String _PJ = "JS-PENDING"; //PROTOCOL - PENDING JOB
    public static String _SCJ = "JS-SCHEDULED"; //PROTOCOL - SCHEDULED JOB
    public static String _AJ = "JS-ADDRESSED"; //PROTOCOL - ADDRESSED JOB
    public static String _JRF = "JS-REQUIRE_FOLLOW_UP"; //PROTOCOL - JOB REQUIRES FOLLOW UP
    public static String _SR_INV = "SR_INVENTORY"; //SEARCH RESULTS - INVENTORY
    public static String _SR_JOB = "SR_JOB"; //SEARCH RESULTS - JOB
    public static String _SR_TECH = "SR_TECH"; //SEARCH RESULTS - TECHNICIAN

    //Persistance variables
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static ArrayList<ACJobObject> OFFLINESCHEDULECACHE = new ArrayList<>();
    private static ACJobObject TEMPJOBCACHE = null;
    public static Integer jobIndex = -1;
    public static String Account = "VOID";
    private static Gson gson = new GsonBuilder().setLenient().create();
    public static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(getServerAddress()+"/api/")
            .client(new OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();
    public static RestInterface service = retrofit.create(RestInterface.class);
    public static String getServerAddress(){
        return NMMUBASEURL;
    }

    public static ACJobObject getCurrentJob(){
        switch (jobIndex){
            case -2:
                return TEMPJOBCACHE;
            case -1:
                return null;
            default:
                return OFFLINESCHEDULECACHE.get(jobIndex);
        }
    }

    public static void setTEMPJOBCACHE(ACJobObject Temp){
        TEMPJOBCACHE = Temp;
        jobIndex = -2;
    }

    public static boolean isCACHEMPTY(){
        return OFFLINESCHEDULECACHE.isEmpty();
    }

    public static ArrayList<ACJobObject> getCACHE(){
        return OFFLINESCHEDULECACHE;
    }

    public static void setCACHE(ArrayList<ACJobObject> CACHEUpdate){
        OFFLINESCHEDULECACHE = CACHEUpdate;
    }



    public static boolean checkToken(){
        return !(Token == "INVALID");
    }

    public static String getToken(){
        return Cryptor.encrypt(Token,CRYPTOKEY);
    }

    public static void setToken(String token){
        Token = Cryptor.decrypt(token,CRYPTOKEY);
    }

    public static String encode64Str(String string){
        if(string != null) {
            byte[] data = new byte[0];
            try {
                data = string.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return Base64.encodeToString(data, Base64.DEFAULT);
        }
        return null;
    }

    public static byte[] encode64(String string){
        if(string != null) {
            byte[] data = new byte[0];
            try {
                data = string.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return Base64.encode(data, Base64.DEFAULT);
        }
        return null;
    }

    public static String decode64(String base64Str){
        if(base64Str != null) {
            byte[] data = Base64.decode(base64Str, Base64.DEFAULT);
            String string = "Decode Unsuccessful";
            try {
                string = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return string;
        }
        else
            return null;
    }

    public static String encodeImgToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeImgBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public static void verifyStoragePermissions(Activity activity) {
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }



}
