package za.ac.nmmu.hons.fsmhons.Models.Communication;

/**
 * Created by danie on 7/22/2016.
 */
public class TokenedObject {
    public String Token;
    public String[] TArray = new String[]{}; //Tokened String array

    public TokenedObject(String token,String[] tArray)
    {
        this.Token = token;
        this.TArray = tArray;
    }

}
