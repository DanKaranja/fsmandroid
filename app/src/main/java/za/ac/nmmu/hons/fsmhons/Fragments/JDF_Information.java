package za.ac.nmmu.hons.fsmhons.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;

import butterknife.BindView;
import butterknife.ButterKnife;
import za.ac.nmmu.hons.fsmhons.Models.ACJobObject;
import za.ac.nmmu.hons.fsmhons.R;
import za.ac.nmmu.hons.fsmhons.Utilities.JobTimer;
import za.ac.nmmu.hons.fsmhons.Utilities.Persistence;

public class JDF_Information extends Fragment implements View.OnClickListener{

    private FragmentListener mListener;
    private JobTimer timerInstance;
    @BindView(R.id.BTN_TrackJob) Button _BTN_TrackJob;
    @BindView(R.id.LBL_TXTJobIdentifier) TextView _LBL_TXTJobIdentifier;
    @BindView(R.id.LBL_TXTSource) TextView _LBL_TXTSource;
    @BindView(R.id.LBL_TXTLocation) TextView _LBL_TXTLocation;
    @BindView(R.id.LBL_TXTReqSkills) TextView _LBL_TXTReqSkills;
    @BindView(R.id.LBL_TXTProblemStatement) TextView _LBL_TXTProblemStatement;
    @BindView(R.id.LBL_TXTDiagnostics) TextView _LBL_TXTDiagnostics;
    @BindView(R.id.LBL_TXTEndDateTime) TextView _LBL_TXTEndDateTime;
    @BindView(R.id.LBL_TXTStartDateTime) TextView _LBL_TXTStartDateTime;
    @BindView(R.id.IVS_JDCPF_pAddress) ImageView _IVS_JDCPF_pAddress;


    public JDF_Information() {
        // Required empty public constructor
    }

    public static JDF_Information newInstance() {
        JDF_Information fragment = new JDF_Information();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_job_information, container, false);
        ButterKnife.bind(this, view);
        ACJobObject CurrentJob = Persistence.getCurrentJob();
        timerInstance = JobTimer.getInstance();
        _LBL_TXTJobIdentifier.setText(CurrentJob.JobIdentifier);
        _LBL_TXTSource.setText(CurrentJob.Source);
        _LBL_TXTLocation.setText(CurrentJob.Location);
        _LBL_TXTReqSkills.setText(CurrentJob.RequiredSkillSet);
        _LBL_TXTProblemStatement.setText(CurrentJob.JobDetails[0]);
        _LBL_TXTDiagnostics.setText(CurrentJob.JobDetails[1]);
        _LBL_TXTEndDateTime.setText(CurrentJob.EndDateTime);
        _LBL_TXTStartDateTime.setText(CurrentJob.StartDateTime);
        _IVS_JDCPF_pAddress.setOnClickListener(this);

        if (timerInstance.isTimed()) { //Current Job is being tracked
            if(timerInstance.getTimerStatus() == 0) {
                _BTN_TrackJob.setText(R.string.BTN_ResumeTrackJob);
            }
            else {
                _BTN_TrackJob.setText(R.string.BTN_PauseTrackJob);
            }
        }
        _BTN_TrackJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrackJob();
            }
        });
        return view;
    }

    private void TrackJob(){
        JobTimer timerInstance = JobTimer.getInstance();
        if(timerInstance.isTimed()) {
            switch (timerInstance.getTimerStatus())
            {
                case 1: {
                    timerInstance.pauseTimer();
                    SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_PauseTimer), SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                    _BTN_TrackJob.setText(R.string.BTN_ResumeTrackJob);
                    break;
                }
                case 0:{
                    timerInstance.resumeTimer();
                    SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_ResumeTimer), SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                    _BTN_TrackJob.setText(R.string.BTN_PauseTrackJob);
                    break;
                }
            }
        }
        else{
            if(Persistence.getCurrentJob().Status.equals(Persistence._SCJ)) { //Check if its an unaddressed job)
                if (timerInstance.startTimer()) {
                    SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_StartTimer), SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                    _BTN_TrackJob.setText(R.string.BTN_PauseTrackJob);
                } else
                    SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_TrackMultipleJobs), SuperToast.Duration.MEDIUM, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
            }
            else
                SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_AddressedJob), SuperToast.Duration.MEDIUM, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();

        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.IVS_JDCPF_pAddress:
            {
                String pAddress = _LBL_TXTLocation.getText().toString();
                if(!(pAddress.equals("-"))) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("google.navigation:q="+pAddress+"=d"));
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(intent);
                    }else
                        SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_FailedMapResolve), SuperToast.Duration.MEDIUM, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();

                }
            }
                break;
        }

    }
}
