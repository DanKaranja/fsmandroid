package za.ac.nmmu.hons.fsmhons.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.hons.fsmhons.Models.Communication.TokenedObject;
import za.ac.nmmu.hons.fsmhons.R;
import za.ac.nmmu.hons.fsmhons.Utilities.Persistence;

public class TechnicianAccountActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;
    private String[] Account;
    private String AccountID;
    @BindView(R.id.LBL_TXTTA_ID) TextView _LBL_TXTTA_ID;
    @BindView(R.id.LBL_TXTTA_Cell) TextView _LBL_TXTTA_Cell;
    @BindView(R.id.LBL_TXTTA_Location) TextView _LBL_TXTTA_Location;
    @BindView(R.id.LBL_TXTTA_SkillSet) TextView _LBL_TXTTA_SkillSet;
    @BindView(R.id.LBL_TXTTA_AddInfo) TextView _LBL_TXTTA_AddInfo;
    @BindView(R.id.IVS_TA_Cell) ImageView _IVS_TA_Cell;
    @BindView(R.id.IVS_TA_Location) ImageView _IVS_TA_Location;
    @BindView(R.id.TA_swipeContainer) SwipeRefreshLayout _TA_swipeContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_technician_account);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(TechnicianAccountActivity.this, R.style.AppTheme_Dark_Dialog);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            SuperToast.create(getBaseContext(), getResources().getString(R.string.response_FailedParameters), SuperToast.Duration.MEDIUM, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
            finish();
        }
        final String[] TQUERY = (String[])extras.getSerializable("TQUERY");
        AccountID = TQUERY[0];
        _TA_swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                RR_getAccoutDetails(TQUERY);
            }
        });
        _TA_swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        RR_getAccoutDetails(TQUERY);

    }
    private void startProcessDialog(String Message) {
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(Message);
        progressDialog.show();
    }
    private void RR_getAccoutDetails(String[] TQUERY) {
        startProcessDialog("Fetching...");
        Persistence.service.GetAccountDetails(new TokenedObject(Persistence.getToken(),TQUERY))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String[]>() {
                    @Override
                    public void onCompleted() {
                        progressDialog.dismiss();
                        _TA_swipeContainer.setRefreshing(false);
                        updateUI();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.w("ACCOUNTOUTPUT",e.getMessage());
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onNext(String[] response) {
                        if(response != null){ //Valid Token
                            Account = response;
                        }
                    }
                });
    }

    private void updateUI(){
        _LBL_TXTTA_ID.setText(Account[0]);
        _LBL_TXTTA_Cell.setText(Account[1]);
        _LBL_TXTTA_Location.setText(Account[2] + "\nLast Updated: "+Account[3]);
        _LBL_TXTTA_SkillSet.setText(Account[4]);
        _LBL_TXTTA_AddInfo.setText(Account[5]);
        _IVS_TA_Cell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(AccountID.equals(Persistence.Account))) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + Account[2]));
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }
            }
        });
        _IVS_TA_Location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((!(Account[3].equals("UNKNOWN"))) || !(AccountID.equals(Persistence.Account))) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("google.navigation:q="+Account[3]+"=d"));
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }else
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.response_FailedMapResolve), SuperToast.Duration.MEDIUM, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();

                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = NavUtils.getParentActivityIntent(this);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                NavUtils.navigateUpTo(this, intent);
                return true;
        }
        // If we got here, the user's action was not recognized.
        // Invoke the superclass to handle it.
        return super.onOptionsItemSelected(item);
    }

}
