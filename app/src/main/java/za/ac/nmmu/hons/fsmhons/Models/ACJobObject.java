package za.ac.nmmu.hons.fsmhons.Models;

import java.util.ArrayList;

/**
 * Created by danie on 7/11/2016.
 */
public class ACJobObject {
    public String Status;
    public String JobID;
    public String JobIdentifier;
    public String Source;
    public String Location;
    public String StartDateTime;
    public String EndDateTime;
    public String RequiredSkillSet;
    public String Priority;
    public String[] JobDetails = new String[] {};
    public String[] ClientDetails = new String[] {};
    public ArrayList<String[]> InventoryRegistry = new ArrayList<>();
    public ArrayList<String[]> ResourcesRegistry = new ArrayList<>();
    public String[] JobReport = new String[]{};



}
