package za.ac.nmmu.hons.fsmhons.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.ArrayList;

import br.com.bemobi.medescope.Medescope;
import br.com.bemobi.medescope.callback.DownloadStatusCallback;
import butterknife.BindView;
import butterknife.ButterKnife;
import za.ac.nmmu.hons.fsmhons.R;
import za.ac.nmmu.hons.fsmhons.Utilities.Persistence;

public class JDF_Documents extends Fragment {

    private RecyclerView mRecyclerView;
    private JobDocumentsRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<String[]> Documents;
    private Medescope mMedescope;
    private FragmentListener mListener;


    public JDF_Documents() {
        // Required empty public constructor
    }

    public static JDF_Documents newInstance() {
        JDF_Documents fragment = new JDF_Documents();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_job_documents, container, false);
        Persistence.verifyStoragePermissions(getActivity());
        mRecyclerView = (RecyclerView) view.findViewById(R.id.JDDF_Recycler);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        Documents = Persistence.getCurrentJob().ResourcesRegistry;
        if(!(Documents.isEmpty()))
            mAdapter = new JobDocumentsRecyclerAdapter(Documents);
        else
            SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_NoDocuments), SuperToast.Duration.SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }
    public class JobDocumentsRecyclerAdapter extends RecyclerView.Adapter<JobDocumentsRecyclerAdapter.ViewHolder> {
        private ArrayList<String[]> Documents;

        public class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.JDI_DRCYV_CardView) CardView _JDI_DRCYV_CardView;
            @BindView(R.id.JDI_DRCYV_DownStatus) ProgressBar _JDI_DRCYV_DownStatus;
            @BindView(R.id.JDI_DRCYV_Name) TextView _JDI_DRCYV_Name;
            @BindView(R.id.JDI_DRCYV_Desc) TextView _JDI_DRCYV_Desc;
            @BindView(R.id.JDI_DRCYV_IV)ImageView _JDI_DRCYV_IV;

            public ViewHolder(View v) {
                super(v);
                ButterKnife.bind(this, v);
            }
        }
        public JobDocumentsRecyclerAdapter(ArrayList<String[]> Documents) {

            this.Documents = Documents;
        }
        @Override
        public JobDocumentsRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_recycler_documents, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            mMedescope = Medescope.getInstance(getActivity().getBaseContext());
            mMedescope.setApplicationName(getString(R.string.app_name));
            final String[] CurrentDocuments = Documents.get(position);
            final Boolean exists = DirContains(CurrentDocuments[2]);
            //Progress bar presets
            holder._JDI_DRCYV_DownStatus.setMax(100);
            holder._JDI_DRCYV_DownStatus.setVisibility(View.GONE);
            holder._JDI_DRCYV_DownStatus.setProgress(0);
            holder._JDI_DRCYV_DownStatus.setIndeterminate(false);

            //The other card objects
            if(exists)
                holder._JDI_DRCYV_IV.setImageResource(R.drawable.ic_action_communication_stay_primary_portrait);
            else
                holder._JDI_DRCYV_IV.setImageResource(R.drawable.ic_action_file_cloud_download);
            holder._JDI_DRCYV_Name.setText(getString(R.string.LBL_DocumentName,CurrentDocuments[0]));
            holder._JDI_DRCYV_Desc.setText(getString(R.string.LBL_DocumentComment, CurrentDocuments[1]));
            holder._JDI_DRCYV_CardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!exists)
                    {
                        NotificationData notificationDatadata = new NotificationData(CurrentDocuments[1], CurrentDocuments[2], getString(R.string.app_name));
                        mMedescope.subscribeStatus(getActivity(),CurrentDocuments[1], new DownloadStatusCallback() {
                            @Override
                            public void onDownloadNotEnqueued(String downloadId) {
                               // SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_DownloadFailedQueue), SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.ORANGE, SuperToast.Animations.FLYIN)).show();
                            }

                            @Override
                            public void onDownloadPaused(String downloadId, int reason) {
                               // SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_DownloadPause) + " Reason: " + reason, SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.ORANGE, SuperToast.Animations.POPUP)).show();
                            }

                            @Override
                            public void onDownloadInProgress(String downloadId, int progress) {
                                holder._JDI_DRCYV_DownStatus.setVisibility(View.VISIBLE);
                                holder._JDI_DRCYV_DownStatus.setProgress(progress);
                                holder._JDI_DRCYV_DownStatus.setIndeterminate(false);
                            }

                            @Override
                            public void onDownloadOnFinishedWithError(String downloadId, int reason, String data) {
                                SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_DownloadFailed) + " Reason: " + reason, SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.ORANGE, SuperToast.Animations.POPUP)).show();
                                holder._JDI_DRCYV_DownStatus.setVisibility(View.GONE);
                                holder._JDI_DRCYV_DownStatus.setProgress(0);
                                holder._JDI_DRCYV_DownStatus.setIndeterminate(false);
                            }

                            @Override
                            public void onDownloadOnFinishedWithSuccess(String downloadId, String filePath, String data) {
                                holder._JDI_DRCYV_DownStatus.setProgress(100);
                                holder._JDI_DRCYV_IV.setImageResource(R.drawable.ic_action_communication_stay_primary_portrait);
                                File from = new File(filePath);
                                File to = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/" + CurrentDocuments[2]);
                                from.renameTo(to);
                                SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_DownloadFinished), SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                                OpenResourceFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/" + CurrentDocuments[2]);
                            }

                            @Override
                            public void onDownloadCancelled(String downloadId) {
                                SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_DownloadCancelled), SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.ORANGE, SuperToast.Animations.POPUP)).show();
                                holder._JDI_DRCYV_DownStatus.setVisibility(View.GONE);
                                holder._JDI_DRCYV_DownStatus.setProgress(0);
                                holder._JDI_DRCYV_DownStatus.setIndeterminate(false);
                            }
                        });
                        mMedescope.enqueue(
                                notificationDatadata.ID, //Download ID
                                Persistence.getServerAddress() + CurrentDocuments[3], // Download Link
                                CurrentDocuments[3], // File Name
                                notificationDatadata.Title,
                                notificationDatadata.toJson());

                    }
                    else{
                        OpenResourceFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/" + CurrentDocuments[2]);
                    }
                }
            });
        }
        @Override
        public int getItemCount() {
            return Documents.size();
        }

        private boolean DirContains(String filename){
            File downDir =  new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath()+"/"+filename);
            if(downDir.exists())
                return true;
            else
                return false;
        }
        public void OpenResourceFile(String filePath){
            try {
                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                File file = new File(filePath);
                int type = -1;
                if(filePath.contains(".pdf"))
                    type = 0;
                if ((filePath.contains(".docx")) || (filePath.contains(".doc")))
                    type = 1;
                if ((filePath.contains(".png"))||(filePath.contains(".jpg"))||(filePath.contains(".jpeg")))
                    type = 2;
                switch (type){
                    case 0: {
                        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                        break;
                    }
                    case 1: {
                        intent.setDataAndType(Uri.fromFile(file), "application/msword");
                        break;
                    }
                    case 2: {
                        intent.setDataAndType(Uri.fromFile(file), "image/*");
                        break;
                    }
                    default :{
                        intent.setDataAndType(Uri.fromFile(file), "*/*");
                        break;
                    }

                }
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } catch (Exception e) {
                SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.response_DownloadFailedOpen), SuperToast.Duration.VERY_SHORT, Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
            }
        }


    }
    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public class NotificationData {

        public String ID;
        public String Title;
        public String Description;


        public NotificationData(String Id) {
            this.ID = ID;
        }

        public NotificationData(String ID, String Title, String Description) {
            this.ID = ID;
            this.Title = Title;
            this.Description = Description;
        }

        public String toJson() {
            Gson gson = new GsonBuilder().create();
            return gson.toJson(this);
        }

    }

}
