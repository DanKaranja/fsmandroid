package za.ac.nmmu.hons.fsmhons.Utilities;

import android.util.Log;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.hons.fsmhons.Models.Communication.HttpResp;
import za.ac.nmmu.hons.fsmhons.Models.Communication.TokenedObject;


/**
 * Created by danie on 8/24/2016.
 */
public class Pinger {
    private Thread TimerThread;
    private final Integer TimerStep = 10000;
    private String[] Strings;
    private static Pinger Instance;
    private boolean Proceed = false;

    private Pinger(){
        Strings = new String[]{"-","-","-"};
        ping();
        TimerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(TimerStep);
                        if(Proceed)
                            ping();
                        Proceed = false;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        TimerThread.start();
        Log.w("PINGEROUTPUT","PINGER STARTED");

    }

    public static Pinger getInstance(){
        if(Instance == null) // No Pinger is running
            Instance = new Pinger();
        return Instance;
    }

    public void update(Double Latitude,Double Longitude,String Time){
        Strings = new String[]{Persistence.Account,Latitude.toString(),Longitude.toString(),Time};
    }

    private void ping() {
        Persistence.service.PingBackend(new TokenedObject(Persistence.getToken(),Strings))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<HttpResp>() {
                    @Override
                    public void onCompleted() {
                        Proceed = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.w("PINGEROUTPUT",e.getMessage());

                    }

                    @Override
                    public void onNext(HttpResp response) {
                        if(response.protocol.equals(Persistence._PRESP))
                            Log.w("PINGEROUTPUT",response.responseString);
                    }
                });
    }
}
